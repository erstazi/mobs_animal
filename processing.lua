local S = mobs.intllib

-- Meat Processing Machine: a device that does all the meat grinding,
-- stuffing and tying process to make sausages. It also takes out blood
-- remains from ground meat for blood sausages.


-- processable meat, output, has blood remains and casing
local processed = {
	{"mobs:blood", "mobs:blood_sausage", false, "mobs:intestines"},
	{"mobs:pork_carcass", "mobs:pork_sausages_raw", true, "mobs:intestines"}
}

meat_processor_formspec = "size[8.97,9.0547722342733]"
	.. "label[0,0;Meat Processing Machine]"
	.. "label[1,1.6;Casing input]"
	.. "label[2.6,1.6;Stuffing input]"
	.. "label[6,1.6;Output]"
	.. "list[current_player;main;0.5,5;8,4;]"
	.. "list[current_name;csrc;1,2.3;1,1;]"
	.. "list[current_name;ssrc;2.6,2.3;1,1;]"
	.. "list[current_name;dst;6,2.3;2,2;]"
	.. "image_button[4.3,2.3;1,1;meat_processor_front.png;start;;false;true;]"

minetest.register_node(":mobs:meat_processor", {
	description = "Meat Processor",
	tiles = {
		"meat_processor_top.png",
		"meat_processor_bottom.png",
		"meat_processor_side.png",
		"meat_processor_side.png",
		"meat_processor_back.png",
		"meat_processor_front.png"
		},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	groups = {oddly_breakable_by_hand=2, cracky=3, dig_immediate=1},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.125, 0.5}, 
			{-0.5, 0.375, -0.3125, 0.125, 0.5, 0.3125}, 
			{-0.4375, 0.3125, -0.25, 0.0625, 0.375, 0.25}, 
			{-0.375, 0.1875, -0.1875, 0, 0.3125, 0.1875}, 
			{-0.3125, 0.125, -0.125, -0.0625, 0.1875, 0.125}, 
		}
	},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("infotext",  "Meat Processing Machine")
		meta:set_string("formspec", meat_processor_formspec)
		meta:set_int("status", 1)
		
		local inv = meta:get_inventory()
		inv:set_size("csrc", 1)
		inv:set_size("ssrc", 1)
		inv:set_size("dst", 4)
		
	end,
	
	can_dig = function( pos, player )
		local meta = minetest.get_meta(pos);
		local inv = meta:get_inventory()

		local name = player:get_player_name()
		if minetest.is_protected(pos, name) then
			minetest.record_protection_violation(pos, name)
			return false
		end
		
		return inv:is_empty('csrc') and inv:is_empty('ssrc') and inv:is_empty('dst')
	end,
	
	on_receive_fields = function(pos, formname, fields, sender)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		
		if fields["start"] then
			if inv:is_empty("csrc") or inv:is_empty("ssrc") then
				return
			end
		
			meta:set_string("infotext", S("Meat Processing Machine (active)"))
			
			local timer = minetest.get_node_timer(pos)
			timer:start(3)
		end
	end,
		
	on_timer = function(pos)
		-- nil check
		local meta = minetest.get_meta(pos)
		if not meta then
			return
		end

		-- empty slots check
		local inv = meta:get_inventory()
		
		if not inv or inv:is_empty("csrc") or inv:is_empty("ssrc") then
			meta:set_int("status", 1)
			meta:set_string("infotext", S("Meat Processing Machine (halted)"))
			return false
		end
		
		-- check if source items are on the processed meat list
		local has_item

		for i = 1, #processed do
			if inv:contains_item("ssrc", ItemStack(processed[i][1])) and inv:contains_item("csrc", ItemStack(processed[i][4])) then
				has_item = i
				break
			end
		end

		if not has_item then
			meta:set_string("infotext", S("Meat Processing Machine (halted)"))
			return false
		end
		
		-- full inventory check
		if not inv:room_for_item("dst", processed[has_item][2]) or not inv:room_for_item("dst", "mobs:blood") then
			meta:set_string("infotext", S("Meat Processing Machine (clogged)"))
			return true
		end
		
		-- do the stuff
		local status = meta:get_int("status")
		local phases = {"grinding", "stuffing", "tying"}
		
		if status < 4 then
			meta:set_string("infotext", S("Meat Processing Machine ("..phases[status]..")"))
			meta:set_int("status", status + 1)
		else
			inv:remove_item("csrc", processed[has_item][4])
			inv:remove_item("ssrc", processed[has_item][1])
			inv:add_item("dst", processed[has_item][2])
			
			if processed[has_item][3] then
				inv:add_item("dst", "mobs:blood")
			end
			
			meta:set_int("status", 1)
		end

		if inv:is_empty("csrc") or inv:is_empty("csrc") then
			meta:set_int("status", 1)
			meta:set_string("infotext", S("Meat Processing Machine (halted)"))
		end

		return true
		
	end
})

-- intestines for casing
minetest.register_craftitem(":mobs:intestines", {
	description = S("Intestines"),
	inventory_image = "mobs_pork_intestines.png",
	groups = {food_intestines = 1, flammable = 2},
})

-- blood
minetest.register_craftitem(":mobs:blood", {
	description = S("Blood"),
	inventory_image = "mobs_blood.png",
	groups = {food_blood = 1}
})

-- blood sausage
minetest.register_craftitem(":mobs:blood_sausage", {
	description = S("Blood Sausage"),
	inventory_image = "mobs_blood_sausage.png",
	groups = {food_sausages = 1, flammable = 2},
	on_use = minetest.item_eat(5)
})
